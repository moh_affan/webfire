<!--Page-->
<div class="page">
    <div class="page-header">
        <h1 class="page-title"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= site_url('/') ?>">Home</a></li>
            <li class="breadcrumb-item active"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></li>
        </ol>
        <div class="page-header-actions">
            <a class="btn btn-sm btn-primary btn-round" href="http://datatables.net" target="_blank">
                <i class="icon md-link" aria-hidden="true"></i>
                <span class="hidden-sm-down">Official Website</span>
            </a>
        </div>
    </div>

    <div class="page-content">
        <!-- Panel Basic -->
        <div class="panel">
            <header class="panel-heading">
                <div class="panel-actions"></div>
                <h3 class="panel-title">Content</h3>
            </header>
            <div class="panel-body">
                <h1>This Page Intentionally Left Blank</h1>

                <p>As of Bonfire <?php echo BONFIRE_VERSION ?>, this page has no default content.</p>

                <p>This is your playground.</p>

                <p>Make it home.</p>
            </div>
        </div>
    </div>
</div>