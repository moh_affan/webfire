<?php

$testSegment = $this->uri->segment(4);
$rolesUrl = site_url(SITE_AREA . '/settings/roles');

?>
<?php if (has_permission('Bonfire.Roles.Add')) : ?>
    <a class="btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon" data-toggle="tooltip"
       data-original-title="<?php echo lang('role_roles'); ?>" href='<?php echo $rolesUrl; ?>'>
        <i class="icon md-view-list-alt" aria-hidden="true"></i>
    </a>

    <a id="create_new" class="btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon" data-toggle="tooltip"
       data-original-title="<?php echo lang('role_new_role'); ?>"
       href='<?php echo "{$rolesUrl}/create"; ?>'>
        <i class="icon md-plus" aria-hidden="true"></i>
    </a>
<?php endif; ?>
<a class="btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon" data-toggle="tooltip"
   data-original-title="<?php echo lang('matrix_header'); ?>"
   href='<?php echo "{$rolesUrl}/permission_matrix"; ?>'>
    <i class="icon fa fa-sitemap" aria-hidden="true"></i>
</a>
