<div class="page">
    <div class="page-header">
        <h1 class="page-title"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= site_url('/') ?>">Home</a></li>
            <li class="breadcrumb-item active"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></li>
        </ol>
        <div class="page-header-actions">
            <?php Template::block('sub_nav') ?>
        </div>
    </div>

    <div class="page-content">
        <!-- Panel Basic -->
        <div class="panel">
            <header class="panel-heading">
                <div class="panel-actions"></div>
                <h3 class="panel-title">Mail Template</h3>
            </header>
            <div class="panel-body">
                <p class="intro"><?php echo lang('emailer_template_note'); ?></p>
                <div class="admin-box">
                    <style scoped='scoped'>
                        .admin-box .template {
                            width: 99%;
                        }
                    </style>
                    <?php echo form_open(SITE_AREA . '/settings/emailer/template'); ?>
                    <fieldset>
                        <legend><?php echo lang('emailer_header'); ?></legend>
                        <div class="clearfix">
                            <div class="input form-material">
                                <textarea name="header" rows="15"
                                          class="template form-control"><?php echo htmlspecialchars_decode($this->load->view('email/_header', null, true)); ?></textarea>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend><?php echo lang('emailer_footer'); ?></legend>
                        <div class="clearfix">
                            <div class="input form-material">
                                <textarea name="footer" rows="15"
                                          class="template form-control"><?php echo htmlspecialchars_decode($this->load->view('email/_footer', null, true)); ?></textarea>
                            </div>
                        </div>
                    </fieldset>
                    <br>
                    <fieldset class="form-actions">
                        <button type="submit" name="save" id="submit" class="btn btn-primary"
                                value="<?php e(lang('emailer_save_template')); ?>">
                            <?php e(lang('emailer_save_template')); ?>
                        </button>
                        <?php echo ' ' . lang('bf_or') . ' ' . anchor(SITE_AREA . '/settings/emailer', lang('bf_action_cancel')); ?>
                    </fieldset>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>