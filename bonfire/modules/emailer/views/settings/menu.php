<ul class="site-sub-menu">
    <li class="site-menu-item">
        <a class="animsition-link" href="<?php echo site_url(SITE_AREA . '/settings/emailer'); ?>">
            <span class="site-menu-title"><?php echo lang('bf_menu_email_settings'); ?></span>
        </a>
    </li>
    <li class="site-menu-item">
        <a class="animsition-link" href="<?php echo site_url(SITE_AREA . '/settings/emailer/template'); ?>">
            <span class="site-menu-title"><?php echo lang('bf_menu_email_template'); ?></span>
        </a>
    </li>
    <li class="site-menu-item">
        <a class="animsition-link" href="<?php echo site_url(SITE_AREA . '/settings/emailer/queue'); ?>">
            <span class="site-menu-title"><?php echo lang('bf_menu_email_queue'); ?></span>
        </a>
    </li>
</ul>