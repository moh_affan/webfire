<a class="btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon" data-toggle="tooltip"
   data-original-title="<?php echo lang('bf_context_settings'); ?>"
   href='<?php echo site_url(SITE_AREA .'/settings/emailer') ?>'>
    <i class="icon md-settings" aria-hidden="true"></i>
</a>

<a class="btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon" data-toggle="tooltip"
   data-original-title="<?php echo lang('emailer_email_template') ?>"
   href='<?php echo site_url(SITE_AREA .'/settings/emailer/template') ?>'>
    <i class="icon md-format-align-justify" aria-hidden="true"></i>
</a>

<a class="btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon" data-toggle="tooltip"
   data-original-title="<?php echo lang('emailer_emailer_queue') ?>"
   href='<?php echo site_url(SITE_AREA .'/settings/emailer/queue') ?>'>
    <i class="icon fa fa-list-ul" aria-hidden="true"></i>
</a>

<a class="btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon" data-toggle="tooltip"
   data-original-title="<?php echo lang('emailer_create_email'); ?>"
   href='<?php echo site_url(SITE_AREA .'/settings/emailer/create')?>'>
    <i class="icon fa fa-paper-plane-o" aria-hidden="true"></i>
</a>