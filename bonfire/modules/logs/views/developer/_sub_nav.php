<?php

$checkSegment = $this->uri->segment(4);
$logsUrl = site_url(SITE_AREA . '/developer/logs');

?>

<a class="btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon" data-toggle="tooltip"
   data-original-title="<?php echo lang('logs_logs'); ?>"
   href='<?php echo $logsUrl; ?>'>
    <i class="icon md-view-list" aria-hidden="true"></i>
</a>

<a class="btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon" data-toggle="tooltip"
   data-original-title="<?php echo lang('logs_settings'); ?>"
   href='<?php echo "{$logsUrl}/settings"; ?>'>
    <i class="icon md-settings" aria-hidden="true"></i>
</a>