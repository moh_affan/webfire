<div class="page">
    <div class="page-header">
        <h1 class="page-title"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= site_url('/') ?>">Home</a></li>
            <li class="breadcrumb-item active"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></li>
        </ol>
        <div class="page-header-actions">
            <?php Template::block('sub_nav') ?>
        </div>
    </div>

    <div class="page-content">
        <!-- Panel Basic -->
        <div class="panel">
            <header class="panel-heading">
                <div class="panel-actions"></div>
                <h3 class="panel-title">Permissions</h3>
            </header>
            <div class="panel-body">
                <?php if ($log_threshold == 0) : ?>
                    <div class="alert alert-warning ">
                        <a class="close" data-dismiss="alert">&times;</a>
                        <?php echo lang('logs_not_enabled'); ?>
                    </div>
                <?php endif; ?>
                <div class="admin-box">
                    <?php echo form_open(site_url(SITE_AREA . '/developer/logs/enable'), 'class=""'); ?>
                    <fieldset>
                        <div class="form-group form-material">
                            <label for="log_threshold"
                                   class="control-label"><?php echo lang('logs_the_following'); ?></label>
                            <div class="">
                                <select name="log_threshold" id="log_threshold" class="form-control">
                                    <option value="0" <?php echo set_select('log_threshold', 0, $log_threshold == 0); ?>><?php echo lang('logs_what_0'); ?></option>
                                    <option value="1" <?php echo set_select('log_threshold', 1, $log_threshold == 1); ?>><?php echo lang('logs_what_1'); ?></option>
                                    <option value="2" <?php echo set_select('log_threshold', 2, $log_threshold == 2); ?>><?php echo lang('logs_what_2'); ?></option>
                                    <option value="3" <?php echo set_select('log_threshold', 3, $log_threshold == 3); ?>><?php echo lang('logs_what_3'); ?></option>
                                    <option value="4" <?php echo set_select('log_threshold', 4, $log_threshold == 4); ?>><?php echo lang('logs_what_4'); ?></option>
                                </select>
                                <p class="help-block"><?php echo lang('logs_what_note'); ?></p>
                            </div>
                        </div>
                    </fieldset>
                    <div class="alert alert-info ">
                        <a class="close" data-dismiss="alert">&times;</a>
                        <?php echo lang('logs_big_file_note'); ?>
                    </div>
                    <fieldset class="form-actions">
                        <button type="submit" name="save" class="btn btn-primary"
                                value="<?php echo lang('logs_save_button'); ?>">
                            <?php echo lang('logs_save_button'); ?>
                        </button>
                    </fieldset>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>