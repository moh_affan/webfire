<?php

$testSegment = $this->uri->segment(4);
$translateUrl = site_url(SITE_AREA . '/developer/translate');

?>

<a class="btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon" data-toggle="tooltip"
   data-original-title="<?php echo lang('translate_translate'); ?>"
   href='<?php echo $translateUrl; ?>'>
    <i class="icon md-translate" aria-hidden="true"></i>
</a>

<a class="btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon" data-toggle="tooltip"
   data-original-title="<?php echo lang('translate_export_short'); ?>"
   href='<?php echo "{$translateUrl}/export"; ?>'>
    <i class="icon md-archive" aria-hidden="true"></i>
</a>