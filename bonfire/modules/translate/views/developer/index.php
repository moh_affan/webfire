<div class="page">
    <div class="page-header">
        <h1 class="page-title"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= site_url('/') ?>">Home</a></li>
            <li class="breadcrumb-item active"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></li>
        </ol>
        <div class="page-header-actions">
            <?php Template::block('sub_nav') ?>
        </div>
    </div>

    <div class="page-content">
        <!-- Panel Basic -->
        <div class="panel">
            <header class="panel-heading">
                <div class="panel-actions"></div>
                <h3 class="panel-title">Translate</h3>
            </header>
            <div class="panel-body">
                <div class="well">
                    <?php echo form_open(current_url(), 'class="form-inline"'); ?>
                    <div class="form-group form-material">
                        <label for='trans_lang'><?php e(lang('translate_current_lang')); ?></label>
                        <select name="trans_lang" id="trans_lang" class="form-control col-md-6">
                            <?php foreach ($languages as $lang) : ?>
                                <option value="<?php e($lang); ?>"<?php echo isset($trans_lang) && $trans_lang == $lang ? ' selected="selected"' : ''; ?>><?php e(ucfirst($lang)); ?></option>
                            <?php endforeach; ?>
                            <option value="other"><?php e(lang('translate_other')); ?></option>
                        </select>
                    </div>
                    <div id='new_lang_field' class="form-group form-material" style='display: none;'>
                        <label for='new_lang'><?php e(lang('translate_new_lang')); ?></label>
                        <input type="text" class="form-control col-md-6" name="new_lang" id="new_lang"
                               value="<?php echo set_value('new_lang'); ?>"/>
                    </div>
                    <button type="submit" name="select_lang" class="btn btn-small btn-primary"
                            value="<?php e(lang('translate_select')); ?>">
                        <?php e(lang('translate_select')); ?>
                    </button>
                    <?php echo form_close(); ?>
                </div>
                <!-- Core -->
                <div class="admin-box">
                    <h3><?php echo lang('translate_core'); ?> <span
                                class="subhead"><?php echo count($lang_files) . ' ' . lang('bf_files'); ?></span></h3>
                    <?php
                    $linkUrl = site_url(SITE_AREA . "/developer/translate/edit/{$trans_lang}");
                    $cnt = 1;
                    $brk = 3;
                    foreach ($lang_files as $file) :
                        if ($cnt == 1) :
                            ?>
                            <div class="row-fluid">
                        <?php
                        endif;
                        ++$cnt;
                        ?>
                        <a class='col-md-1' href='<?php echo "{$linkUrl}/{$file}"; ?>'><?php e($file); ?></a>
                        <?php
                        if ($cnt > $brk) :
                            ?>
                            </div>
                            <?php
                            $cnt = 1;
                        endif;
                    endforeach;
                    if ($cnt != 1) :
                    ?>
                </div>
                <?php endif; ?>
            </div>
            <!-- Modules -->
            <div class="admin-box">
                <h3><?php echo lang('translate_modules') . ((!empty($modules) && is_array($modules)) ? ' <span class="subhead">' . count($modules) . ' ' . lang('bf_files') . '</span>' : ''); ?></h3>
                <?php
                if (!empty($modules) && is_array($modules)) :
                $linkUrl = site_url(SITE_AREA . "/developer/translate/edit/{$trans_lang}");
                $cnt = 1;
                $brk = 3;
                foreach ($modules as $file) :
                    if ($cnt == 1) :
                        ?>
                        <div class="row-fluid">
                    <?php
                    endif;
                    $cnt++;
                    ?>
                    <a class='col-md-4' href="<?php echo "{$linkUrl}/{$file}"; ?>"><?php e($file); ?></a>
                    <?php if ($cnt > $brk) : ?>
                    </div>
                    <?php
                    $cnt = 1;
                endif;
                endforeach;
                if ($cnt != 1) :
                ?>
            </div>
            <?php
            endif;
            else :
                ?>
                <div class="alert alert-info ">
                    <a class="close" data-dismiss="alert">&times;</a>
                    <?php echo lang('translate_no_modules'); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
</div>
</div>