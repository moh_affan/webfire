<div class="page">
    <div class="page-header">
        <h1 class="page-title"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= site_url('/') ?>">Home</a></li>
            <li class="breadcrumb-item active"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></li>
        </ol>
        <div class="page-header-actions">
            <?php Template::block('sub_nav') ?>
        </div>
    </div>

    <div class="page-content">
        <!-- Panel Basic -->
        <div class="panel">
            <header class="panel-heading">
                <div class="panel-actions"></div>
                <h3 class="panel-title">Translate</h3>
            </header>
            <div class="panel-body">
                <div class="admin-box">
                    <?php
                    if (! empty($orig) && is_array($orig)) :
                        echo form_open(current_url(), 'class="" id="translate_form"');
                        ?>
                        <input type="hidden" name="trans_lang" value="<?php e($trans_lang); ?>" />
                        <fieldset>
                            <legend>
                                <?php if (count($orig) > 30) : ?>
                                    <button class="gobottom pull-right btn btn-small"><span class="md-long-arrow-down"></span></button>
                                <?php endif; ?>
                                <h3><?php echo lang('translate_file'); ?>: <span class='filename'><?php echo $lang_file; ?></span></h3>
                            </legend>
                            <table class='table table-striped'>
                                <thead>
                                <tr>
                                    <th class='column-check'><div class="checkbox-custom checkbox-primary"><input class='check-all' type='checkbox' /><label></label></div></th>
                                    <th class='text-right'><?php echo ucwords($orig_lang); ?></th>
                                    <th><?php echo ucwords($trans_lang); ?></th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <td colspan='3'>
                                        <?php
                                        if ($orig_lang != $trans_lang) :
                                            echo lang('bf_with_selected');
                                            ?>
                                            <button type="submit" name="translate" class="btn translate-sel" value="<?php echo lang('translate_translate'); ?>" ><?php echo lang('translate_translate'); ?></button>
                                        <?php
                                        endif;
                                        if (count($orig) > 30) :
                                            ?>
                                            <button class="gotop pull-right btn btn-small"><span class="md-long-arrow-up"></span></button>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                </tfoot>
                                <tbody>
                                <?php foreach ($orig as $key => $val) : ?>
                                    <tr>
                                        <td class='column-check'><div class="checkbox-custom checkbox-primary"><input type='checkbox' name='checked[]' value="<?php echo $key; ?>" <?php echo in_array($key, $chkd) ? "checked='checked' " : ''; ?>/><label></label></div></td>
                                        <td><label class="form-control-label" for="lang<?php echo $key; ?>"><?php e($val); ?></label></td>
                                        <td class="form-group form-material">
                                            <?php if (strlen($val) < 80) : ?>
                                                <input type="text" class="input-xxlarge form-control" name="lang[<?php echo $key; ?>]" id="lang<?php echo $key; ?>" value="<?php e(isset($new[$key]) ? $new[$key] : ''); ?>" />
                                            <?php else : ?>
                                                <textarea class="input-xxlarge form-control" name="lang[<?php echo $key; ?>]" id="lang<?php echo $key; ?>"><?php e(isset($new[$key]) ? $new[$key] : ''); ?></textarea>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </fieldset>
                        <fieldset class="form-actions">
                            <button type="submit" name="save" class="btn btn-primary" value="<?php e(lang('bf_action_save')); ?>"><?php e(lang('bf_action_save')); ?></button> <?php e(lang('bf_or')); ?>
                            <a href="<?php
                            echo site_url(SITE_AREA . '/developer/translate/index') . '/';
                            e($trans_lang);
                            ?>"><?php e(lang('bf_action_cancel')); ?></a>
                        </fieldset>
                        <?php
                        echo form_close();
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>