<div class="page">
    <div class="page-header">
        <h1 class="page-title"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= site_url('/') ?>">Home</a></li>
            <li class="breadcrumb-item active"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></li>
        </ol>
        <div class="page-header-actions">
            <?php Template::block('sub_nav') ?>
        </div>
    </div>

    <div class="page-content">
        <!-- Panel Basic -->
        <div class="panel">
            <header class="panel-heading">
                <div class="panel-actions"></div>
                <h3 class="panel-title">Translate</h3>
            </header>
            <div class="panel-body">
                <p class="intro"><?php echo lang('translate_export_note'); ?></p>
                <div class='admin-box'>
                    <?php echo form_open(current_url(), 'class=""'); ?>
                    <fieldset>
                        <div class="form-group form-material">
                            <label for="export_lang"
                                   class="form-control-label"><?php echo lang('translate_language'); ?></label>
                            <div class="controls">
                                <select name="export_lang" id="export_lang" class="form-control col-md-6">
                                    <?php foreach ($languages as $lang) : ?>
                                        <option value="<?php e($lang); ?>" <?php echo isset($trans_lang) && $trans_lang == $lang ? 'selected="selected"' : '' ?>><?php e(ucfirst($lang)); ?></option>
                                    <?php endforeach; ?>
                                    <option value="other"><?php e(lang('translate_other')); ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-material ">
                            <label class="form-control-label"><?php echo lang('translate_include'); ?></label>
                            <div class="checkbox-custom checkbox-primary">
                                <input type="checkbox" id="include_core" name="include_core" value="1"
                                       checked="checked"/>
                                <label for="include_core">
                                    <?php echo lang('translate_include_core'); ?>
                                </label>
                                <input type="checkbox" id="include_mods" name="include_mods" value="1"/>
                                <label for="include_mods">
                                    <?php echo lang('translate_include_mods'); ?>
                                </label>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="form-actions">
                        <button type="submit" name="export" class="btn btn-primary"
                                value="<?php e(lang('translate_export_short')); ?>">
                            <?php e(lang('translate_export_short')); ?>
                        </button>
                    </fieldset>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>