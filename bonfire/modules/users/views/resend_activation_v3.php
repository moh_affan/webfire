<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">

    <title><?php
        echo isset($page_title) ? "{$page_title} : " : '';
        e(class_exists('Settings_lib') ? settings_item('site.title') : 'Bonfire');
        ?></title>

    <!--<link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="../../assets/images/favicon.ico">-->

    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?= Template::theme_url("css/bootstrap4.min.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("css/bootstrap-extend.min.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("css/site.min.css") ?>">

    <!-- Plugins -->
    <link rel="stylesheet" href="<?= Template::theme_url("vendor/animsition/animsition.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("vendor/asscrollable/asScrollable.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("vendor/switchery/switchery.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("vendor/intro-js/introjs.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("vendor/slidepanel/slidePanel.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("vendor/flag-icon-css/flag-icon.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("vendor/waves/waves.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("css/pages/forgot-password.css") ?>">

    <!-- Fonts -->
    <link rel="stylesheet" href="<?= Template::theme_url("fonts/material-design/material-design.min.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("fonts/brand-icons/brand-icons.min.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("fonts/Roboto/Roboto.min.css") ?>">

    <!--[if lt IE 9]>
    <script src="<?=Template::theme_url('vendor/html5shiv/html5shiv.min.js')?>"></script>
    <![endif]-->

    <!--[if lt IE 10]>
    <script src="<?=Template::theme_url('vendor/media-match/media.match.min.js')?>"></script>
    <script src="<?=Template::theme_url('vendor/respond/respond.min.js')?>"></script>
    <![endif]-->

    <!-- Scripts -->
    <script src="<?= Template::theme_url("vendor/breakpoints/breakpoints.js") ?>"></script>
    <script>
        Breakpoints();
    </script>
</head>
<body class="animsition page-forgot-password layout-full">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->


<!-- Page -->
<div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
    <div class="page-content vertical-align-middle">
        <h2><?php echo lang('us_activate_resend'); ?></h2>
        <?php if (validation_errors()) : ?>
            <div class="alert alert-error ">
                <?php echo validation_errors(); ?>
            </div>
        <?php else: ?>
            <p><?php echo lang('us_activate_resend_note'); ?></p>
        <?php endif; ?>
        <?php echo form_open($this->uri->uri_string(), array('autocomplete' => 'off', 'role' => 'form')); ?>
        <div class="form-group form-material floating <?php echo iif( form_error('password') , 'error') ;?>" data-plugin="formMaterial">
            <input type="email" class="form-control empty" id="email" name="email" value="<?php echo set_value('email') ?>">
            <label class="floating-label" for="email"><?php echo lang('bf_email'); ?></label>
        </div>
        <div class="form-group">
            <button class="btn btn-primary btn-block" type="submit" name="send" value="<?php echo lang('us_activate_code_send') ?>">
                <?php echo lang('us_activate_code_send') ?>
            </button>
        </div>
        <?php echo form_close() ?>

        <footer class="page-copyright">
            <p>WEBSITE BY Creation Studio</p>
            <p>© 2018. All RIGHT RESERVED.</p>
            <div class="social">
                <a class="btn btn-icon btn-pure" href="javascript:void(0)">
                    <i class="icon bd-twitter" aria-hidden="true"></i>
                </a>
                <a class="btn btn-icon btn-pure" href="javascript:void(0)">
                    <i class="icon bd-facebook" aria-hidden="true"></i>
                </a>
                <a class="btn btn-icon btn-pure" href="javascript:void(0)">
                    <i class="icon bd-google-plus" aria-hidden="true"></i>
                </a>
            </div>
        </footer>
    </div>
</div>
<!-- End Page -->


<!-- Core  -->
<script src="<?= Template::theme_url('vendor/babel-external-helpers/babel-external-helpers.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/jquery/jquery.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/popper-js/umd/popper.min.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/bootstrap/bootstrap.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/animsition/animsition.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/mousewheel/jquery.mousewheel.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/asscrollbar/jquery-asScrollbar.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/asscrollable/jquery-asScrollable.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/ashoverscroll/jquery-asHoverScroll.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/waves/waves.js') ?>"></script>

<!-- Plugins -->
<script src="<?= Template::theme_url('vendor/switchery/switchery.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/intro-js/intro.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/screenfull/screenfull.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/slidepanel/jquery-slidePanel.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/jquery-placeholder/jquery.placeholder.js') ?>"></script>

<!-- Scripts -->
<script src="<?= Template::theme_url('js/Component.js') ?>"></script>
<script src="<?= Template::theme_url('js/Plugin.js') ?>"></script>
<script src="<?= Template::theme_url('js/Base.js') ?>"></script>
<script src="<?= Template::theme_url('js/Config.js') ?>"></script>

<script src="<?= Template::theme_url('js/Section/Menubar.js') ?>"></script>
<script src="<?= Template::theme_url('js/Section/GridMenu.js') ?>"></script>
<script src="<?= Template::theme_url('js/Section/Sidebar.js') ?>"></script>
<script src="<?= Template::theme_url('js/Section/PageAside.js') ?>"></script>
<script src="<?= Template::theme_url('js/Plugin/menu.js') ?>"></script>

<script src="<?= Template::theme_url('js/config/colors.js') ?>"></script>
<script src="<?= Template::theme_url('js/config/tour.js') ?>"></script>

<!-- Page -->
<script src="<?= Template::theme_url('js/Site.js') ?>"></script>
<script src="<?= Template::theme_url('js/Plugin/asscrollable.js') ?>"></script>
<script src=" <?= Template::theme_url('js/Plugin/slidepanel.js') ?>"></script>
<script src="<?= Template::theme_url('js/Plugin/switchery.js') ?>"></script>
<script src=" <?= Template::theme_url('js/Plugin/jquery-placeholder.js') ?>"></script>
<script src="<?= Template::theme_url('js/Plugin/material.js') ?>"></script>

<script>
    (function (document, window, $) {
        'use strict';
        var Site = window.Site;
        $(document).ready(function () {
            Site.run();
        });
    })(document, window, jQuery);
</script>

</body>
</html>
