<?php
$site_open = $this->settings_lib->item('auth.allow_register');
?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">

    <title><?php
        echo isset($page_title) ? "{$page_title} : " : '';
        e(class_exists('Settings_lib') ? settings_item('site.title') : 'Bonfire');
        ?></title>

    <!--<link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="../../assets/images/favicon.ico">-->

    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?= Template::theme_url("css/bootstrap4.min.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("css/bootstrap-extend.min.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("css/site.min.css") ?>">

    <!-- Plugins -->
    <link rel="stylesheet" href="<?= Template::theme_url("vendor/animsition/animsition.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("vendor/asscrollable/asScrollable.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("vendor/switchery/switchery.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("vendor/intro-js/introjs.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("vendor/slidepanel/slidePanel.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("vendor/flag-icon-css/flag-icon.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("vendor/waves/waves.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("css/pages/login-v3.css") ?>">

    <!-- Fonts -->
    <link rel="stylesheet" href="<?= Template::theme_url("fonts/material-design/material-design.min.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("fonts/brand-icons/brand-icons.min.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("fonts/Roboto/Roboto.min.css") ?>">

    <!--[if lt IE 9]>
    <script src="<?=Template::theme_url('vendor/html5shiv/html5shiv.min.js')?>"></script>
    <![endif]-->

    <!--[if lt IE 10]>
    <script src="<?=Template::theme_url('vendor/media-match/media.match.min.js')?>"></script>
    <script src="<?=Template::theme_url('vendor/respond/respond.min.js')?>"></script>
    <![endif]-->

    <!-- Scripts -->
    <script src="<?= Template::theme_url("vendor/breakpoints/breakpoints.js") ?>"></script>
    <script>
        Breakpoints();
    </script>
</head>
<body class="animsition page-login-v3 layout-full">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->


<!-- Page -->
<div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">>
    <div class="page-content vertical-align-middle">
        <div class="panel">
            <div class="panel-body">
                <div class="brand">
                    <img class="brand-img" src="<?= Template::theme_url('images/logo-colored.png') ?>" alt="...">
                    <h2 class="brand-text font-size-18"><?php e(class_exists('Settings_lib') ? settings_item('site.title') : 'Bonfire') ?></h2>
                </div>
                <?php echo Template::message(); ?>

                <?php
                if (validation_errors()) :
                    ?>
                    <div class="row-fluid">
                        <div class="col-md-12">
                            <div class="alert alert-error">
                                <a data-dismiss="alert" class="close">&times;</a>
                                <?php echo validation_errors(); ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <?php echo form_open(LOGIN_URL, array('autocomplete' => 'off')); ?>
                    <div class="form-group form-material floating <?php echo iif(form_error('login'), 'error'); ?>"
                         data-plugin="formMaterial">
                        <input type="text" class="form-control" name="login" id="login_value"
                               value="<?php echo set_value('login'); ?>" tabindex="1"
                               placeholder="<?php echo $this->settings_lib->item('auth.login_type') == 'both' ? lang('bf_username') . '/' . lang('bf_email') : ucwords($this->settings_lib->item('auth.login_type')) ?>"/>
                        <label class="floating-label"><?php echo $this->settings_lib->item('auth.login_type') == 'both' ? lang('bf_username') . '/' . lang('bf_email') : ucwords($this->settings_lib->item('auth.login_type')) ?></label>
                    </div>
                    <div class="form-group form-material floating" data-plugin="formMaterial">
                        <input class="form-control" type="password" name="password" id="password" value=""
                               tabindex="2"
                               placeholder="<?php echo lang('bf_password'); ?>"/>
                        <label class="floating-label">Password</label>
                    </div>
                    <div class="form-group clearfix">
                        <?php if ($this->settings_lib->item('auth.allow_remember')) : ?>
                            <div class="checkbox-custom checkbox-inline checkbox-primary checkbox-lg float-left">
                                <input type="checkbox" name="remember_me" id="remember_me" value="1">
                                <label for="remember_me"><?php echo lang('us_remember_note'); ?></label>
                            </div>
                        <?php endif; ?>
                        <a class="float-right"
                           href="<?= site_url('forgot_password') ?>"><?= lang('us_forgot_your_password') ?></a>
                    </div>
                    <button type="submit" name="log-me-in" id="submit" value="<?php e(lang('us_let_me_in')); ?>"
                            class="btn btn-primary btn-block btn-lg mt-40"><?php e(lang('us_let_me_in')); ?></button>
                <?php echo form_close() ?>
                <?php if ($site_open) : ?>
                    <p>Still no account? Please <?php echo anchor(REGISTER_URL, ucfirst(strtolower(lang('us_sign_up')))); ?></p>
                <?php endif; ?>
                <?php // show for Email Activation (1) only
                if ($this->settings_lib->item('auth.user_activation_method') == 1) : ?>
                    <!-- Activation Block -->
                    <p style="text-align: left" class="well">
                        <?php echo lang('bf_login_activate_title'); ?><br/>
                        <?php
                        $activate_str = str_replace('[ACCOUNT_ACTIVATE_URL]', anchor('/activate', lang('bf_activate')), lang('bf_login_activate_email'));
                        $activate_str = str_replace('[ACTIVATE_RESEND_URL]', anchor('/resend_activation', lang('bf_activate_resend')), $activate_str);
                        echo $activate_str; ?>
                    </p>
                <?php endif; ?>
            </div>
        </div>

        <footer class="page-copyright page-copyright-inverse">
            <p>WEBSITE BY Creation Studio</p>
            <p>© 2018. All RIGHT RESERVED.</p>
            <div class="social">
                <a class="btn btn-icon btn-pure" href="javascript:void(0)">
                    <i class="icon bd-twitter" aria-hidden="true"></i>
                </a>
                <a class="btn btn-icon btn-pure" href="javascript:void(0)">
                    <i class="icon bd-facebook" aria-hidden="true"></i>
                </a>
                <a class="btn btn-icon btn-pure" href="javascript:void(0)">
                    <i class="icon bd-google-plus" aria-hidden="true"></i>
                </a>
            </div>
        </footer>
    </div>
</div>
<!-- End Page -->


<!-- Core  -->
<script src="<?= Template::theme_url('vendor/babel-external-helpers/babel-external-helpers.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/jquery/jquery.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/popper-js/umd/popper.min.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/bootstrap/bootstrap.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/animsition/animsition.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/mousewheel/jquery.mousewheel.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/asscrollbar/jquery-asScrollbar.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/asscrollable/jquery-asScrollable.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/ashoverscroll/jquery-asHoverScroll.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/waves/waves.js') ?>"></script>

<!-- Plugins -->
<script src="<?= Template::theme_url('vendor/switchery/switchery.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/intro-js/intro.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/screenfull/screenfull.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/slidepanel/jquery-slidePanel.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/jquery-placeholder/jquery.placeholder.js') ?>"></script>

<!-- Scripts -->
<script src="<?= Template::theme_url('js/Component.js') ?>"></script>
<script src="<?= Template::theme_url('js/Plugin.js') ?>"></script>
<script src="<?= Template::theme_url('js/Base.js') ?>"></script>
<script src="<?= Template::theme_url('js/Config.js') ?>"></script>

<script src="<?= Template::theme_url('js/Section/Menubar.js') ?>"></script>
<script src="<?= Template::theme_url('js/Section/GridMenu.js') ?>"></script>
<script src="<?= Template::theme_url('js/Section/Sidebar.js') ?>"></script>
<script src="<?= Template::theme_url('js/Section/PageAside.js') ?>"></script>
<script src="<?= Template::theme_url('js/Plugin/menu.js') ?>"></script>

<script src="<?= Template::theme_url('js/config/colors.js') ?>"></script>
<script src="<?= Template::theme_url('js/config/tour.js') ?>"></script>

<!-- Page -->
<script src="<?= Template::theme_url('js/Site.js') ?>"></script>
<script src="<?= Template::theme_url('js/Plugin/asscrollable.js') ?>"></script>
<script src=" <?= Template::theme_url('js/Plugin/slidepanel.js') ?>"></script>
<script src="<?= Template::theme_url('js/Plugin/switchery.js') ?>"></script>
<script src=" <?= Template::theme_url('js/Plugin/jquery-placeholder.js') ?>"></script>
<script src="<?= Template::theme_url('js/Plugin/material.js') ?>"></script>

<script>
    (function (document, window, $) {
        'use strict';
        var Site = window.Site;
        $(document).ready(function () {
            Site.run();
        });
    })(document, window, jQuery);
</script>

</body>
</html>
