<?php if (has_permission('Bonfire.Users.Manage')): ?>
    <a class="btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon" data-toggle="tooltip"
       data-original-title="<?php echo lang('bf_users'); ?>"
       href='<?php echo site_url(SITE_AREA . '/settings/users') ?>'>
        <i class="icon md-view-list-alt" aria-hidden="true"></i>
    </a>

    <a id="create_new" class="btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon" data-toggle="tooltip"
       data-original-title="<?php echo lang('bf_new') . ' ' . lang('bf_user'); ?>"
       href='<?php echo site_url(SITE_AREA . '/settings/users/create') ?>'>
        <i class="icon md-plus" aria-hidden="true"></i>
    </a>
<?php endif; ?>