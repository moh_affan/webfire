<?php

$errorClass = empty($errorClass) ? ' error' : $errorClass;
$controlClass = empty($controlClass) ? 'span6' : $controlClass;
$fieldData = array(
    'errorClass' => $errorClass,
    'controlClass' => $controlClass,
);

?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">

    <title><?php
        echo isset($page_title) ? "{$page_title} : " : '';
        e(class_exists('Settings_lib') ? settings_item('site.title') : 'Bonfire');
        ?></title>

    <!--<link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="../../assets/images/favicon.ico">-->

    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?= Template::theme_url("css/bootstrap4.min.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("css/bootstrap-extend.min.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("css/site.min.css") ?>">

    <!-- Plugins -->
    <link rel="stylesheet" href="<?= Template::theme_url("vendor/animsition/animsition.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("vendor/asscrollable/asScrollable.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("vendor/switchery/switchery.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("vendor/intro-js/introjs.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("vendor/slidepanel/slidePanel.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("vendor/flag-icon-css/flag-icon.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("vendor/waves/waves.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("css/pages/register-v3.css") ?>">

    <!-- Fonts -->
    <link rel="stylesheet" href="<?= Template::theme_url("fonts/material-design/material-design.min.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("fonts/brand-icons/brand-icons.min.css") ?>">
    <link rel="stylesheet" href="<?= Template::theme_url("fonts/Roboto/Roboto.min.css") ?>">

    <!--[if lt IE 9]>
    <script src="<?=Template::theme_url('vendor/html5shiv/html5shiv.min.js')?>"></script>
    <![endif]-->

    <!--[if lt IE 10]>
    <script src="<?=Template::theme_url('vendor/media-match/media.match.min.js')?>"></script>
    <script src="<?=Template::theme_url('vendor/respond/respond.min.js')?>"></script>
    <![endif]-->

    <!-- Scripts -->
    <script src="<?= Template::theme_url("vendor/breakpoints/breakpoints.js") ?>"></script>
    <script>
        Breakpoints();
    </script>
</head>
<body class="animsition page-register-v3 layout-full">
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->


<!-- Page -->
<div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">>
    <div class="page-content vertical-align-middle">
        <div class="panel">
            <div class="panel-body">
                <div class="brand">
                    <img class="brand-img" src="<?= Template::theme_url('images/logo-colored.png') ?>" alt="...">
                    <h2 class="brand-text font-size-18"><?php e(class_exists('Settings_lib') ? settings_item('site.title') : 'Bonfire') ?></h2>
                </div>
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-error ">
                        <?php echo validation_errors(); ?>
                    </div>
                <?php endif; ?>
                <div class="alert alert-info ">
                    <?php
                    if (isset($password_hints)) {
                        echo $password_hints;
                    }
                    ?>
                </div>
                <?php echo form_open(REGISTER_URL, array('autocomplete' => 'off')); ?>
                <fieldset>
                    <?php Template::block('user_fields', 'user_fields', $fieldData); ?>
                </fieldset>
                <fieldset>
                    <?php
                    // Allow modules to render custom fields. No payload is passed
                    // since the user has not been created, yet.
                    Events::trigger('render_user_form');
                    ?>
                    <!-- Start of User Meta -->
                    <?php
                    $v = $this->load->view('users/user_meta', array('frontend_only' => true),true);
                    $v = str_replace('col-md-6', 'col-md-12', $v);
                    echo $v;
                    ?>
                    <!-- End of User Meta -->
                </fieldset>
                <button type="submit" name="register" id="submit" value="<?php echo lang('us_register'); ?>"
                        class="btn btn-primary btn-block btn-lg mt-40"><?php echo lang('us_register'); ?></button>
                <?php echo form_close() ?>
                <p><?php echo lang('us_already_registered'); ?>
                    Please <?php echo anchor(LOGIN_URL, lang('bf_action_login')); ?></p>
            </div>
        </div>

        <footer class="page-copyright page-copyright-inverse">
            <p>WEBSITE BY Creation Studio</p>
            <p>© 2018. All RIGHT RESERVED.</p>
            <div class="social">
                <a class="btn btn-icon btn-pure" href="javascript:void(0)">
                    <i class="icon bd-twitter" aria-hidden="true"></i>
                </a>
                <a class="btn btn-icon btn-pure" href="javascript:void(0)">
                    <i class="icon bd-facebook" aria-hidden="true"></i>
                </a>
                <a class="btn btn-icon btn-pure" href="javascript:void(0)">
                    <i class="icon bd-google-plus" aria-hidden="true"></i>
                </a>
            </div>
        </footer>
    </div>
</div>
<!-- End Page -->


<!-- Core  -->
<script src="<?= Template::theme_url('vendor/babel-external-helpers/babel-external-helpers.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/jquery/jquery.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/popper-js/umd/popper.min.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/bootstrap/bootstrap.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/animsition/animsition.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/mousewheel/jquery.mousewheel.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/asscrollbar/jquery-asScrollbar.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/asscrollable/jquery-asScrollable.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/ashoverscroll/jquery-asHoverScroll.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/waves/waves.js') ?>"></script>

<!-- Plugins -->
<script src="<?= Template::theme_url('vendor/switchery/switchery.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/intro-js/intro.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/screenfull/screenfull.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/slidepanel/jquery-slidePanel.js') ?>"></script>
<script src="<?= Template::theme_url('vendor/jquery-placeholder/jquery.placeholder.js') ?>"></script>

<!-- Scripts -->
<script src="<?= Template::theme_url('js/Component.js') ?>"></script>
<script src="<?= Template::theme_url('js/Plugin.js') ?>"></script>
<script src="<?= Template::theme_url('js/Base.js') ?>"></script>
<script src="<?= Template::theme_url('js/Config.js') ?>"></script>

<script src="<?= Template::theme_url('js/Section/Menubar.js') ?>"></script>
<script src="<?= Template::theme_url('js/Section/GridMenu.js') ?>"></script>
<script src="<?= Template::theme_url('js/Section/Sidebar.js') ?>"></script>
<script src="<?= Template::theme_url('js/Section/PageAside.js') ?>"></script>
<script src="<?= Template::theme_url('js/Plugin/menu.js') ?>"></script>

<script src="<?= Template::theme_url('js/config/colors.js') ?>"></script>
<script src="<?= Template::theme_url('js/config/tour.js') ?>"></script>

<!-- Page -->
<script src="<?= Template::theme_url('js/Site.js') ?>"></script>
<script src="<?= Template::theme_url('js/Plugin/asscrollable.js') ?>"></script>
<script src=" <?= Template::theme_url('js/Plugin/slidepanel.js') ?>"></script>
<script src="<?= Template::theme_url('js/Plugin/switchery.js') ?>"></script>
<script src=" <?= Template::theme_url('js/Plugin/jquery-placeholder.js') ?>"></script>
<script src="<?= Template::theme_url('js/Plugin/material.js') ?>"></script>

<script>
    (function (document, window, $) {
        'use strict';
        var Site = window.Site;
        $(document).ready(function () {
            Site.run();
        });
    })(document, window, jQuery);
</script>

</body>
</html>
