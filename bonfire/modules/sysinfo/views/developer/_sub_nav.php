<?php

$checkSegment = $this->uri->segment(4);
$baseUrl = site_url(SITE_AREA . '/developer/sysinfo');

?>
<a class="btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon" data-toggle="tooltip"
   data-original-title="<?php echo lang('sysinfo_system'); ?>"
   href='<?php echo $baseUrl; ?>'>
    <i class="icon md-laptop" aria-hidden="true"></i>
</a>

<a class="btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon" data-toggle="tooltip"
   data-original-title="<?php echo lang('sysinfo_modules'); ?>"
   href='<?php echo "{$baseUrl}/modules"; ?>'>
    <i class="icon md-puzzle-piece" aria-hidden="true"></i>
</a>

<a class="btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon" data-toggle="tooltip"
   data-original-title="<?php echo lang('sysinfo_php'); ?>"
   href='<?php echo "{$baseUrl}/php_info"; ?>'>
    <i class="icon md-code" aria-hidden="true"></i>
</a>