<div class="page">
    <div class="page-header">
        <h1 class="page-title"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= site_url('/') ?>">Home</a></li>
            <li class="breadcrumb-item active"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></li>
        </ol>
        <div class="page-header-actions">
            <?php Template::block('sub_nav') ?>
        </div>
    </div>

    <div class="page-content">
        <!-- Panel Basic -->
        <div class="panel">
            <header class="panel-heading">
                <div class="panel-actions"></div>
                <h3 class="panel-title">PHP Info</h3>
            </header>
            <div class="panel-body">
                <div class="admin-box">
                    <div class="nav-tabs-horizontal tabbable" data-plugin="tabs">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item" role="presentation"><a class="nav-link active" href='#sysinfoVersion'
                                                                        data-toggle='tab'>Version</a></li>
                            <li class="nav-item" role="presentation"><a class="nav-link" href='#sysinfoConfig'
                                                                        data-toggle='tab'>Configuration</a></li>
                            <li class="nav-item" role="presentation"><a class="nav-link" href='#sysinfoCredits'
                                                                        data-toggle='tab'>Credits</a></li>
                        </ul>
                        <div class="tab-content main-settings">
                            <?php echo $phpinfo; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>