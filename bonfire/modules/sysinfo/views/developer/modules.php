<div class="page">
    <div class="page-header">
        <h1 class="page-title"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= site_url('/') ?>">Home</a></li>
            <li class="breadcrumb-item active"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></li>
        </ol>
        <div class="page-header-actions">
            <?php Template::block('sub_nav') ?>
        </div>
    </div>

    <div class="page-content">
        <!-- Panel Basic -->
        <div class="panel">
            <header class="panel-heading">
                <div class="panel-actions"></div>
                <h3 class="panel-title">Modules</h3>
            </header>
            <div class="panel-body">
                <div class="admin-box">
                    <h3><?php echo lang('sysinfo_installed_mods'); ?></h3>
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th><?php echo lang('sysinfo_mod_name'); ?></th>
                            <th><?php echo lang('sysinfo_mod_ver'); ?></th>
                            <th><?php echo lang('sysinfo_mod_desc'); ?></th>
                            <th><?php echo lang('sysinfo_mod_author'); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($modules as $module => $config) : ?>
                            <tr>
                                <td><?php echo $config['name']; ?></td>
                                <td><?php echo $config['version']; ?></td>
                                <td><?php echo $config['description']; ?></td>
                                <td><?php echo $config['author']; ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>