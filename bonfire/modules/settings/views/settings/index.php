<?php

$errorClass = isset($errorClass) ? $errorClass : ' error';
$showExtendedSettings = !empty($extended_settings);
if ($showExtendedSettings) {
    $defaultCountry = 'US';
    $defaultState = '';
    $countryFieldId = false;
    $stateFieldId = false;
}

if (validation_errors()) :
    ?>
    <div class="alert alert-block alert-error fade in">
        <a class="close" data-dismiss="alert">&times;</a>
        <?php echo validation_errors(); ?>
    </div>
<?php endif; ?>
<style>
    .tab-content.main-settings {
        padding-bottom: 9px;
        border-bottom: 1px solid #ddd;
    }

    #name-change-settings input {
        width: 2em;
    }

    #password_iterations {
        width: auto;
    }
</style>

<!--Page-->
<div class="page">
    <div class="page-header">
        <h1 class="page-title"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= site_url('/') ?>">Home</a></li>
            <li class="breadcrumb-item active"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></li>
        </ol>
        <div class="page-header-actions">
            <?php Template::block('sub_nav') ?>
        </div>
    </div>

    <div class="page-content">
        <!-- Panel Basic -->
        <div class="panel">
            <header class="panel-heading">
                <div class="panel-actions"></div>
                <h3 class="panel-title">Settings</h3>
            </header>
            <div class="panel-body">
                <div class="admin-box">
                    <?php echo form_open($this->uri->uri_string(), 'class=""'); ?>
                    <div class="nav-tabs-horizontal tabbable" data-plugin="tabs">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item" role="presentation">
                                <a href="#main-settings" class="nav-link active"
                                   data-toggle="tab"><?php echo lang('set_tab_settings'); ?></a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" href="#security"
                                   data-toggle="tab"><?php echo lang('set_tab_security'); ?></a>
                            </li>
                            <?php if ($showDeveloperTab) : ?>
                                <li class="nav-item" role="presentation"><a href="#developer" class="nav-link"
                                                                            data-toggle="tab"><?php echo lang('set_tab_developer'); ?></a>
                                </li>
                            <?php
                            endif;
                            if ($showExtendedSettings) :
                                ?>
                                <li class="nav-item" role="presentation"><a class="nav-link" href="#extended"
                                                                            data-toggle="tab"><?php echo lang('set_tab_extended'); ?></a>
                                </li>
                            <?php endif; ?>
                        </ul>
                        <div class="tab-content main-settings">
                            <div class="tab-pane active" id="main-settings" role="tabpanel">
                                <?php Template::block('settingsMain', 'settings/settings/index/main', array('errorClass' => $errorClass)); ?>
                            </div>
                            <div class="tab-pane" id="security" role="tabpanel">
                                <?php Template::block('settingsSecurity', 'settings/settings/index/security', array('errorClass' => $errorClass)); ?>
                            </div>
                            <?php if ($showDeveloperTab) : ?>
                                <div class="tab-pane" id="developer" role="tabpanel">
                                    <?php Template::block('settingsDeveloper', 'settings/settings/index/developer', array('errorClass' => $errorClass)); ?>
                                </div>
                            <?php
                            endif;
                            if ($showExtendedSettings) :
                                ?>
                                <div class='tab-pane' id='extended' role="tabpanel">
                                    <?php
                                    Template::block(
                                        'settingsExtended',
                                        'settings/settings/index/extended',
                                        array(
                                            'errorClass' => $errorClass,
                                            'extendedSettings' => $extended_settings,
                                            'defaultCountry' => $defaultCountry,
                                            'defaultState' => $defaultState,
                                            'countryFieldId' => $countryFieldId,
                                            'stateFieldId' => $stateFieldId,
                                        )
                                    );
                                    ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <fieldset class="form-actions" style="padding-top: 10px">
                        <button type="submit" name="save" class="btn btn-primary"
                                value="<?php echo lang('bf_action_save') . ' ' . lang('bf_context_settings'); ?>">
                            <?php echo lang('bf_action_save') . ' ' . lang('bf_context_settings'); ?>
                        </button>
                    </fieldset>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>