<section class="page-aside-section">
    <h5 class="page-aside-title"><?php echo lang('mb_tools'); ?></h5>
    <div class="list-group">
        <a class="list-group-item" href="<?php echo site_url(SITE_AREA . '/developer/builder/create_module'); ?>"><i class="icon md-puzzle-piece" aria-hidden="true"></i> <?php echo lang('mb_mod_builder'); ?></a>
        <a class="list-group-item" href="<?php echo site_url(SITE_AREA . '/developer/builder/create_context'); ?>"><i class="icon md-8tracks" aria-hidden="true"></i> <?php echo lang('mb_new_context'); ?></a>
    </div>
</section>