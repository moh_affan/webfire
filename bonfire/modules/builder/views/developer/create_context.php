<div class="page">
    <div class="page-header">
        <h1 class="page-title"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= site_url('/') ?>">Home</a></li>
            <li class="breadcrumb-item active"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></li>
        </ol>
        <div class="page-header-actions">
            <?php Template::block('sub_nav') ?>
        </div>
    </div>

    <div class="page-content">
        <!-- Panel Basic -->
        <div class="panel">
            <header class="panel-heading">
                <div class="panel-actions"></div>
                <h3 class="panel-title">Context</h3>
            </header>
            <div class="panel-body">
                <p class='intro'><?php echo lang('mb_context_create_intro'); ?></p>
                <p class='intro'><?php echo lang('mb_context_create_intro_note'); ?></p>
                <div class="admin-box">
                    <?php if (validation_errors()) : ?>
                        <div class="alert alert-error">
                            <a data-dismiss="alert" class="close">&times;</a>
                            <h4 class="alert-heading"><?php echo lang('mb_form_errors'); ?></h4>
                            <?php echo validation_errors(); ?>
                        </div>
                    <?php endif; ?>
                    <?php echo form_open(current_url(), 'class=""'); ?>
                    <fieldset>
                        <div class="form-group form-material <?php echo form_error('context_name') ? ' error' : ''; ?>">
                            <label for="context_name"
                                   class="form-control-label"><?php echo lang('mb_context_name'); ?></label>
                            <div class="">
                                <input type="text" name="context_name" id="context_name"
                                       class="input-large form-control col-md-4"
                                       value="<?php echo settings_item('context_name'); ?>"/>
                                <span class="help-inline"><?php
                                    echo form_error('context_name') ? form_error('context_name') . '<br />' : '';
                                    echo lang('mb_context_name_help');
                                    ?></span>
                            </div>
                        </div>
                        <?php if (!empty($roles) && is_array($roles)) : ?>
                            <div class="form-group form-material ">
                                <label class="form-control-label"
                                       id="roles_label"><?php echo lang('mb_roles_label'); ?></label>
                                <div class="checkbox-custom checkbox-primary" aria-labelledby="roles_label"
                                     role="group">
                                    <?php foreach ($roles as $role) : ?>
                                        <input type="checkbox" name="roles[]"
                                               id="roles_<?php echo $role->role_id; ?>"
                                               value="<?php echo $role->role_id; ?>" <?php echo set_checkbox('roles[]', $role->role_id); ?> />
                                        <label class="checkbox" for="roles_<?php echo $role->role_id; ?>">
                                            <?php echo $role->role_name; ?>
                                        </label>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="form-group form-material ">
                            <div class="checkbox-custom checkbox-primary">
                                <input type="checkbox" name="migrate" id="migrate"
                                       value="1" <?php echo set_checkbox('migrate', '1'); ?> />
                                <label class="checkbox" for="migrate">
                                    <?php echo lang('mb_context_migrate'); ?>
                                </label>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="form-actions">
                        <button type="submit" name="build" class="btn btn-primary"
                                value="<?php echo lang('mb_context_submit'); ?>">
                            <?php echo lang('mb_context_submit'); ?>
                        </button>
                        <?php
                        echo anchor(
                            site_url(SITE_AREA . '/developer/builder'),
                            '<span class="icon-white icon-ban-circle"></span>&nbsp;' . htmlspecialchars(lang('bf_action_cancel')),
                            array('class' => 'btn btn-warning')
                        );
                        ?>
                    </fieldset>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>