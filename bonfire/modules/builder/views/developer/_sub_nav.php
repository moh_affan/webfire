
<a class="btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon" data-toggle="tooltip"
   data-original-title="<?php echo lang('bf_action_list'); ?>"
   href='<?php echo site_url(SITE_AREA . '/developer/builder') ?>'>
    <i class="icon md-view-list-alt" aria-hidden="true"></i>
</a>

<a id="create_new" class="btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon" data-toggle="tooltip"
   data-original-title="<?php echo lang('mb_new_module'); ?>"
   href='<?php echo site_url(SITE_AREA . '/developer/builder/create_module') ?>'>
    <i class="icon md-plus" aria-hidden="true"></i>
</a>

<a id="create_new_context" class="btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon" data-toggle="tooltip"
   data-original-title="<?php echo lang('mb_new_context'); ?>"
   href='<?php echo site_url(SITE_AREA . '/developer/builder/create_context') ?>'>
    <i class="icon fa fa-plus-circle" aria-hidden="true"></i>
</a>