<?php

$fieldEntries = '';
for ($counter = 1; $field_total >= $counter; $counter++) {
    if (set_value("view_field_label$counter") == null) {
        continue; // Move onto next iteration of the loop
    }

    $field_label = set_value("view_field_label$counter");
    $field_name  = set_value("view_field_name$counter");

    $fieldEntries .= "
\$lang['{$module_name_lower}_field_{$field_name}'] = '{$field_label}';";
}

echo $lang = "<?php defined('BASEPATH') || exit('No direct script access allowed');
" . PHP_EOL . "
\$lang['{$module_name_lower}_manage']      = 'Kelola {$module_name}';
\$lang['{$module_name_lower}_edit']        = 'Sunting';
\$lang['{$module_name_lower}_true']        = 'Benar';
\$lang['{$module_name_lower}_false']       = 'Salah';
\$lang['{$module_name_lower}_create']      = 'Buat';
\$lang['{$module_name_lower}_list']        = 'Daftar';
\$lang['{$module_name_lower}_new']       = 'Baru';
\$lang['{$module_name_lower}_edit_text']     = 'Sunting bagian ini sesuai kebutuhan Anda';
\$lang['{$module_name_lower}_no_records']    = 'Tidak ada {$module_name_lower} di dalam sistem.';
\$lang['{$module_name_lower}_create_new']    = 'Buat {$module_name} baru.';
\$lang['{$module_name_lower}_create_success']  = '{$module_name} telah sukses dibuat.';
\$lang['{$module_name_lower}_create_failure']  = 'Ada masalah saat membuat {$module_name_lower}: ';
\$lang['{$module_name_lower}_create_new_button'] = 'Buat {$module_name} baru';
\$lang['{$module_name_lower}_invalid_id']    = 'ID {$module_name} tidak sah.';
\$lang['{$module_name_lower}_edit_success']    = '{$module_name} telah sukses disimpan.';
\$lang['{$module_name_lower}_edit_failure']    = 'Ada masalah saat menyimpan {$module_name_lower}: ';
\$lang['{$module_name_lower}_delete_success']  = 'data telah berhasil dihapus.';
\$lang['{$module_name_lower}_delete_failure']  = 'Kami tidak dapat menghapus data: ';
\$lang['{$module_name_lower}_delete_error']    = 'Ada belum memilih data apapun untuk dihapus.';
\$lang['{$module_name_lower}_actions']     = 'Aksi';
\$lang['{$module_name_lower}_cancel']      = 'Batal';
\$lang['{$module_name_lower}_delete_record']   = 'Hapus {$module_name} ini';
\$lang['{$module_name_lower}_delete_confirm']  = 'Apakah Anda yakin ingin menghapus {$module_name_lower} ini?';
\$lang['{$module_name_lower}_edit_heading']    = 'Sunting {$module_name}';

// Create/Edit Buttons
\$lang['{$module_name_lower}_action_edit']   = 'Simpan {$module_name}';
\$lang['{$module_name_lower}_action_create']   = 'Buat {$module_name}';

// Activities
\$lang['{$module_name_lower}_act_create_record'] = 'Data telah dibuat dengan ID';
\$lang['{$module_name_lower}_act_edit_record'] = 'Data telah disunting dengan ID';
\$lang['{$module_name_lower}_act_delete_record'] = 'Data telah dihapus dengan ID';

//Listing Specifics
\$lang['{$module_name_lower}_records_empty']    = 'Tidak ada data yang ditemukan sesuai yang Anda pili.';
\$lang['{$module_name_lower}_errors_message']    = 'Silahkan perbaiki kesalahan berikut:';

// Column Headings
\$lang['{$module_name_lower}_column_created']  = 'Telah Dibuat';
\$lang['{$module_name_lower}_column_deleted']  = 'Telah Dihapus';
\$lang['{$module_name_lower}_column_modified'] = 'Telah Diubah';
\$lang['{$module_name_lower}_column_deleted_by'] = 'Telah Dihapus oleh';
\$lang['{$module_name_lower}_column_created_by'] = 'Telah Dibuat oleh';
\$lang['{$module_name_lower}_column_modified_by'] = 'Telah Diubah oleh';

// Module Details
\$lang['{$module_name_lower}_module_name'] = '{$module_name}';
\$lang['{$module_name_lower}_module_description'] = '{$module_description}';
\$lang['{$module_name_lower}_area_title'] = '{$module_name}';

// Fields{$fieldEntries}";