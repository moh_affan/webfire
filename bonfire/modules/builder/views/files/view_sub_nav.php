<?php

$controller_name_lower = strtolower($controller_name);
$ucModuleName = preg_replace("/[ -]/", "_", ucfirst($module_name));
$ucControllerName = ucfirst($controller_name);

$createPermission = "{$ucModuleName}.{$ucControllerName}.Create";

//------------------------------------------------------------------------------
// Output the view
//------------------------------------------------------------------------------
echo "<?php
\$areaUrl = SITE_AREA . '/{$controller_name_lower}/{$module_name_lower}';
?>
<a class='btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon' data-toggle='tooltip'
   data-original-title=\"<?php echo lang('{$module_name_lower}_list'); ?>\"
   href=\"<?php echo site_url(\$areaUrl); ?>\">
    <i class='icon md-view-list-alt' aria-hidden='true'></i>
</a>
<?php if (\$this->auth->has_permission('{$createPermission}')) : ?>
<a id='create_new' class='btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon' data-toggle='tooltip'
   data-original-title=\"<?php echo lang('{$module_name_lower}_new'); ?>\"
   href=\"<?php echo site_url(\$areaUrl . '/create'); ?>\">
    <i class='icon md-plus' aria-hidden='true'></i>
</a>
<?php endif; ?>
";