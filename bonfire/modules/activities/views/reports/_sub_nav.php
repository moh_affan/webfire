<?php

$checkSegment = $this->uri->segment(4);
$activitiesReportsUrl = site_url(SITE_AREA . '/reports/activities');
$pageUser = 'activity_user';
$pageModule = 'activity_module';
$pageDate = 'activity_date';

?>
<ul class="nav nav-pills">
    <a class="btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon" data-toggle="tooltip"
       data-original-title="<?php echo lang('activities_home'); ?>" href='<?php echo $activitiesReportsUrl ?>'>
        <i class="icon md-home" aria-hidden="true"></i>
    </a>
    <?php if ($hasPermissionViewUser || $hasPermissionViewOwn) : ?>
        <a class="btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon" data-toggle="tooltip"
           data-original-title="<?php echo lang(str_replace('activity_', 'activities_', $pageUser)); ?>"
           href='<?php echo "{$activitiesReportsUrl}/{$pageUser}" ?>'>
            <i class="icon fa fa-users" aria-hidden="true"></i>
        </a>
    <?php
    endif;
    if ($hasPermissionViewModule) :
        ?>
        <a class="btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon" data-toggle="tooltip"
           data-original-title="<?php echo lang(str_replace('activity_', 'activities_', $pageModule)); ?>"
           href='<?php echo "{$activitiesReportsUrl}/{$pageModule}" ?>'>
            <i class="icon fa fa-puzzle-piece" aria-hidden="true"></i>
        </a>
    <?php
    endif;
    if ($hasPermissionViewDate) :
        ?>
        <a class="btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon" data-toggle="tooltip"
           data-original-title="<?php echo lang(str_replace('activity_', 'activities_', $pageDate)); ?>"
           href='<?php echo "{$activitiesReportsUrl}/{$pageDate}" ?>'>
            <i class="icon fa fa-calendar" aria-hidden="true"></i>
        </a>
    <?php endif; ?>
</ul>