<?php

$hasPermissionDeleteDate = isset($hasPermissionDeleteDate) ? $hasPermissionDeleteDate : false;
$hasPermissionDeleteModule = isset($hasPermissionDeleteModule) ? $hasPermissionDeleteModule : false;
$hasPermissionDeleteUser = isset($hasPermissionDeleteUser) ? $hasPermissionDeleteUser : false;

?>


<!--Page-->
<div class="page">
    <div class="page-header">
        <h1 class="page-title"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= site_url('/') ?>">Home</a></li>
            <li class="breadcrumb-item active"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></li>
        </ol>
        <div class="page-header-actions">
            <?php Template::block('sub_nav') ?>
        </div>
    </div>

    <div class="page-content">
        <!-- Panel Basic -->
        <div class="panel">
            <header class="panel-heading">
                <div class="panel-actions"></div>
                <h3 class="panel-title">Activities</h3>
            </header>
            <div class="panel-body">
                <div class="box select admin-box">
                    <?php echo form_open(SITE_AREA . "/reports/activities/{$vars['which']}", 'class="constrained"'); ?>
                    <fieldset>
                        <legend><?php echo lang('activities_filter_head'); ?></legend>
                        <?php
                        echo form_dropdown(
                            array(
                                'name' => "{$vars['which']}_select",
                                'id' => "{$vars['which']}_select",
                                'class' => 'col-md-3 form-control',
                            ),
                            $select_options,
                            $filter,
                            lang('activities_filter_head'),
                            '',
                            '<span class="help-inline">' . sprintf(
                                lang('activities_filter_note'),
                                $vars['view_which'] == ucwords(lang('activities_date')) ? lang('activities_filter_from_before') : lang('activities_filter_only_for'),
                                strtolower($vars['view_which'])
                            ) . '</span>'
                        );
                        ?>
                    </fieldset>
                    <fieldset class="form-actions">
                        <button type="submit" name="filter" value="Filter" class="btn btn-primary" >Filter</button>
                        <?php
                        if ($vars['which'] == 'activity_own' && $hasPermissionDeleteOwn) :
                            ?>
                            <button type="submit" name="delete" class="btn btn-danger" id="delete-activity_own"><span
                                        class="fa fa-trash text-light"></span>&nbsp;<?php echo lang('activities_own_delete'); ?>
                            </button>
                        <?php elseif ($vars['which'] == 'activity_user' && $hasPermissionDeleteUser) : ?>
                            <button type="submit" name="delete" class="btn btn-danger" id="delete-activity_user"><span
                                        class="fa fa-trash text-light"></span>&nbsp;<?php echo lang('activities_user_delete'); ?>
                            </button>
                        <?php elseif ($vars['which'] == 'activity_module' && $hasPermissionDeleteModule) : ?>
                            <button type="submit" name="delete" class="btn btn-danger" id="delete-activity_module"><span
                                        class="fa fa-trash text-light"></span>&nbsp;<?php echo lang('activities_module_delete'); ?>
                            </button>
                        <?php elseif ($vars['which'] == 'activity_date' && $hasPermissionDeleteDate) : ?>
                            <button type="submit" name="delete" class="btn btn-danger" id="delete-activity_date"><span
                                        class="fa fa-trash text-light"></span>&nbsp;<?php echo lang('activities_date_delete'); ?>
                            </button>
                        <?php endif; ?>
                    </fieldset>
                    <?php echo form_close(); ?>
                </div>
                <h2><?php
                    echo sprintf(
                        lang('activities_view'),
                        $vars['view_which'] == ucwords(lang('activities_date')) ? sprintf(lang('activities_view_before'), $vars['view_which']) : $vars['view_which'],
                        $vars['name']
                    );
                    ?></h2>
                <?php if (empty($activity_content)) : ?>
                    <div class="alert alert-error fade in">
                        <a class="close" data-dismiss="alert">&times;</a>
                        <h4 class="alert-heading"><?php echo lang('activities_not_found'); ?></h4>
                    </div>
                <?php else : ?>
                    <div id="user_activities">
                        <table class="table table-striped table-bordered" id="flex_table">
                            <thead>
                            <tr>
                                <th><?php echo lang('activities_user'); ?></th>
                                <th><?php echo lang('activities_activity'); ?></th>
                                <th><?php echo lang('activities_module'); ?></th>
                                <th><?php echo lang('activities_when'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($activity_content as $activity) : ?>
                                <tr>
                                    <td><span class="icon-user"></span>&nbsp;<?php e($activity->username); ?></td>
                                    <td><?php echo $activity->activity; ?></td>
                                    <td><?php echo $activity->module; ?></td>
                                    <td><?php echo user_time(strtotime($activity->created), null, 'M j, Y g:i A'); ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <?php
                    echo $this->pagination->create_links();
                endif; ?>
            </div>
        </div>
    </div>
</div>