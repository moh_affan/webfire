<?php

if (!function_exists('relative_time')) {
    $this->load->helper('date');
}

?>
<!--Page-->
<div class="page">
    <div class="page-header">
        <h1 class="page-title"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= site_url('/') ?>">Home</a></li>
            <li class="breadcrumb-item active"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></li>
        </ol>
        <div class="page-header-actions">
            <?php Template::block('sub_nav') ?>
        </div>
    </div>

    <div class="page-content">
        <!-- Panel Basic -->
        <div class="panel">
            <header class="panel-heading">
                <div class="panel-actions"></div>
                <h3 class="panel-title">Content</h3>
            </header>
            <div class="panel-body">
                <h3><?php echo lang('us_access_logs'); ?></h3>
                <?php if (!empty($activities) && is_array($activities)) : ?>
                    <ul class="clean">
                        <?php
                        // Determine which field is displayed for the user's identity.
                        $identityField = $this->settings_lib->item('auth.login_type') == 'email' ? 'email' : 'username';
                        foreach ($activities as $activity) :
                            ?>
                            <li>
                                <span class="small"><?php echo relative_time(strtotime($activity->created_on)); ?></span><br/>
                                <strong><?php e($activity->{$identityField}); ?></strong> <?php echo $activity->activity; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php
                else :
                    echo lang('us_no_access_message');
                endif;
                ?>
            </div>
        </div>
    </div>
</div>