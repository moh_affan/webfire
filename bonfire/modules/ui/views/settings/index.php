<!--Page-->
<div class="page">
    <div class="page-header">
        <h1 class="page-title"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= site_url('/') ?>">Home</a></li>
            <li class="breadcrumb-item active"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></li>
        </ol>
        <div class="page-header-actions">
            <?php Template::block('sub_nav') ?>
        </div>
    </div>

    <div class="page-content">
        <!-- Panel Basic -->
        <div class="panel">
            <header class="panel-heading">
                <div class="panel-actions"></div>
                <h3 class="panel-title">Activities</h3>
            </header>
            <div class="panel-body">
                <?php if (validation_errors()) : ?>
                    <div class="alert alert-error fade in">
                        <a class="close" data-dismiss="alert">&times;</a>
                        <p><?php echo validation_errors(); ?></p>
                    </div>
                <?php endif; ?>
                <p class='intro'><?php echo lang('ui_keyboard_shortcuts'); ?></p>
                <div class="admin-box">
                    <?php echo form_open($this->uri->uri_string(), array('class' => "", 'id' => 'shortcut_form')); ?>
                    <table class="table table-striped table-condensed">
                        <thead>
                        <tr>
                            <th><?php echo lang('ui_action'); ?></th>
                            <th colspan="2">
                                <?php echo lang('ui_shortcut'); ?>
                                <span class="help-inline"><?php echo lang('ui_shortcut_help'); ?></span>
                            </th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <td colspan='3' class='form-actions'>
                                <button type="submit" name="save" class="btn btn-primary"
                                        value="<?php echo lang('ui_update_shortcuts'); ?>">
                                    <?php echo lang('ui_update_shortcuts'); ?>
                                </button>
                            </td>
                        </tr>
                        </tfoot>
                        <tbody>
                        <tr>
                            <th class="form-material">
                                <select name="new_action" class="form-control">
                                    <?php
                                    foreach ($current as $name => $detail) :
                                        if (!array_key_exists($name, $settings)) :
                                            ?>
                                            <option value="<?php echo $name; ?>" <?php echo set_select('new_action', $name); ?>>
                                                <?php echo $detail['description']; ?>
                                            </option>
                                        <?php
                                        endif;
                                    endforeach;
                                    ?>
                                </select>
                            </th>
                            <td class="form-material"><input type="text" name="new_shortcut" class="medium form-control"
                                                             value="<?php echo set_value('new_shortcut'); ?>"/></td>
                            <td class="form-material">
                                <button type="submit" name="add_shortcut" class="btn"
                                        value="<?php echo lang('ui_add_shortcut'); ?>">
                                    <?php echo lang('ui_add_shortcut'); ?>
                                </button>
                            </td>
                        </tr>
                        <?php foreach ($settings as $action => $shortcut) : ?>
                            <tr>
                                <th><?php echo $current[$action]['description']; ?></th>
                                <td class="form-material"><input class="form-control" type="text"
                                                                 name="shortcut_<?php echo $action; ?>"
                                                                 value="<?php echo set_value("shortcut_$action", $shortcut); ?>"/>
                                </td>
                                <td>
                                    <button type="submit" name="remove_shortcut[<?php echo $action; ?>]"
                                            value="<?php echo lang('ui_remove_shortcut'); ?>" class="btn btn-danger">
                                        <?php echo lang('ui_remove_shortcut'); ?>
                                    </button>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>