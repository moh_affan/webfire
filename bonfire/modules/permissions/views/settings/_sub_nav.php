<?php

$testSegment = $this->uri->segment(4);
$settingsUrl = site_url(SITE_AREA . '/settings');

?>
<a class="btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon" data-toggle="tooltip"
   data-original-title="<?php echo lang('bf_action_list'); ?>" href='<?php echo "{$settingsUrl}/permissions"; ?>'>
    <i class="icon md-view-list-alt" aria-hidden="true"></i>
</a>

<a id="create_new" class="btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon" data-toggle="tooltip"
   data-original-title="<?php echo lang('bf_action_create'); ?>"
   href='<?php echo "{$settingsUrl}/permissions/create"; ?>'>
    <i class="icon md-plus" aria-hidden="true"></i>
</a>

<a class="btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon" data-toggle="tooltip"
   data-original-title="<?php echo lang('permissions_matrix'); ?>"
   href='<?php echo "{$settingsUrl}/roles/permission_matrix"; ?>'>
    <i class="icon fa fa-sitemap" aria-hidden="true"></i>
</a>