<!--Page-->
<div class="page">
    <div class="page-header">
        <h1 class="page-title"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= site_url('/') ?>">Home</a></li>
            <li class="breadcrumb-item active"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></li>
        </ol>
        <div class="page-header-actions">
            <?php Template::block('sub_nav') ?>
        </div>
    </div>

    <div class="page-content">
        <!-- Panel Basic -->
        <div class="panel">
            <header class="panel-heading">
                <div class="panel-actions"></div>
                <h3 class="panel-title">Permissions</h3>
            </header>
            <div class="panel-body">
                <div class="admin-box">
                    <?php echo form_open($this->uri->uri_string(), 'class=""'); ?>
                    <fieldset>
                        <legend><?php echo lang('permissions_details') ?></legend>

                        <div class="form-group form-material col-md-6 <?php echo form_error('name') ? ' error' : ''; ?>"
                             data-plugin="formMaterial">
                            <label for="name" class="form-control-label"><?php echo lang('permissions_name'); ?></label>
                            <input id="name" type="text" name="name" class="input-large form-control" maxlength="30"
                                   value="<?php echo set_value('name', isset($permissions->name) ? $permissions->name : ''); ?>"/>
                            <span class="help-inline"><?php echo form_error('name'); ?></span>
                        </div>
                        <div class="form-group form-material col-md-6 <?php echo form_error('description') ? ' error' : ''; ?>"
                             data-plugin="formMaterial">
                            <label for="description"
                                   class="form-control-label"><?php echo lang('permissions_description'); ?></label>
                            <input id="description" type="text" name="description" maxlength="100" class="form-control"
                                   value="<?php echo set_value('description', isset($permissions->description) ? $permissions->description : ''); ?>"/>
                            <span class="help-inline"><?php echo form_error('description'); ?></span>
                        </div>
                        <div class="form-group form-material col-md-6" data-plugin="formMaterial">
                            <label for="status"
                                   class="form-control-label"><?php echo lang('permissions_status'); ?></label>
                            <select name="status" id="status" class="form-control">
                                <option value="active" <?php echo set_select('status', 'active', isset($permissions->status) && $permissions->status == 'active'); ?>><?php echo lang('permissions_active'); ?></option>
                                <option value="inactive" <?php echo set_select('status', 'inactive', isset($permissions->status) && $permissions->status == 'inactive'); ?>><?php echo lang('permissions_inactive'); ?></option>
                            </select>
                        </div>
                    </fieldset>
                    <fieldset class='form-actions'>
                        <button type="submit" name="save" class="btn btn-primary"
                                value="<?php echo lang('permissions_save'); ?>"><?php echo lang('permissions_save'); ?></button>
                        <?php
                        echo lang('bf_or') . ' ' . anchor(SITE_AREA . '/settings/permissions', lang('bf_action_cancel'));
                        ?>
                    </fieldset>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>