<!--Page-->
<div class="page">
    <div class="page-header">
        <h1 class="page-title"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= site_url('/') ?>">Home</a></li>
            <li class="breadcrumb-item active"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></li>
        </ol>
        <div class="page-header-actions">
            <?php Template::block('sub_nav')?>
        </div>
    </div>

    <div class="page-content">
        <!-- Panel Basic -->
        <div class="panel">
            <header class="panel-heading">
                <div class="panel-actions"></div>
                <h3 class="panel-title">Permissions</h3>
            </header>
            <div class="panel-body">
                <?php

                $num_columns = 5;

                ?>
                <div class="admin-box">
                    <p class="intro"><?php e(lang('permissions_intro')); ?></p>
                    <?php
                    if (isset($results) && is_array($results) && count($results)) :
                        echo form_open($this->uri->uri_string());
                        ?>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th class="column-check"><input class="check-all" type="checkbox"/></th>
                                <th><?php echo lang('permissions_id'); ?></th>
                                <th><?php echo lang('permissions_name'); ?></th>
                                <th><?php echo lang('permissions_description'); ?></th>
                                <th><?php echo lang('permissions_status'); ?></th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <td colspan="<?php echo $num_columns; ?>">
                                    <?php echo lang('bf_with_selected') ?>
                                    <button type="submit" name="delete" class="btn btn-danger" id="delete-me"
                                            value="<?php echo lang('bf_action_delete') ?>"
                                            onclick="return confirm('<?php e(js_escape(lang('permissions_delete_confirm'))); ?>')">
                                        <?php echo lang('bf_action_delete') ?>
                                    </button>
                                </td>
                            </tr>
                            </tfoot>
                            <tbody>
                            <?php foreach ($results as $record) : ?>
                                <tr>
                                    <td class="column-check"><input type="checkbox" name="checked[]"
                                                                    value="<?php echo $record->permission_id; ?>"/></td>
                                    <td><?php echo $record->permission_id; ?></td>
                                    <td>
                                        <a href='<?php echo site_url(SITE_AREA . "/settings/permissions/edit/{$record->permission_id}"); ?>'><?php e($record->name); ?></a>
                                    </td>
                                    <td><?php e($record->description); ?></td>
                                    <td><?php e(ucfirst($record->status)); ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php
                        echo form_close();
                    else :
                        ?>
                        <p><?php echo lang('permissions_no_records'); ?></p>
                    <?php
                    endif;
                    echo $this->pagination->create_links();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>