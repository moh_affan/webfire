<div class="page">
    <div class="page-header">
        <h1 class="page-title"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= site_url('/') ?>">Home</a></li>
            <li class="breadcrumb-item active"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></li>
        </ol>
        <div class="page-header-actions">
            <?php Template::block('sub_nav') ?>
        </div>
    </div>

    <div class="page-content">
        <!-- Panel Basic -->
        <div class="panel">
            <header class="panel-heading">
                <div class="panel-actions"></div>
                <h3 class="panel-title">Restore</h3>
            </header>
            <div class="panel-body">
                <div class='admin-box restore'>
                    <?php if (empty($results)) : ?>
                        <h3><?php echo sprintf(lang('database_restore_file'), $filename); ?></h3>
                        <div class="alert alert-warning">
                            <?php echo lang('database_restore_attention'); ?>
                        </div>
                        <?php echo form_open($this->uri->uri_string()); ?>
                        <input type="hidden" name="filename" value="<?php echo $filename; ?>" />
                        <fieldset class="form-actions">
                            <button type="submit" name="restore" class="btn btn-primary" value="<?php echo lang('database_restore'); ?>" >
                                <?php echo lang('database_restore'); ?>
                            </button>
                            <?php echo ' ' . lang('bf_or') . ' ' . anchor(SITE_AREA . '/developer/database/backups', lang('bf_action_cancel')); ?>
                        </fieldset>
                        <?php
                        echo form_close();
                    else :
                        ?>
                        <h3><?php echo lang('database_restore_results'); ?></h3>
                        <div class='backups-link'>
                            <?php echo anchor(SITE_AREA . '/developer/database/backups', lang('database_back_to_tools')); ?>
                        </div>
                        <div class="content-box">
                            <p><?php echo $results; ?></p>
                        </div>
                        <div class='backups-link'>
                            <?php echo anchor(SITE_AREA . '/developer/database/backups', lang('database_back_to_tools')); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>