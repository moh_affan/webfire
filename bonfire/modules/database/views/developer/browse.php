<div class="page">
    <div class="page-header">
        <h1 class="page-title"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= site_url('/') ?>">Home</a></li>
            <li class="breadcrumb-item active"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></li>
        </ol>
        <div class="page-header-actions">
            <?php Template::block('sub_nav') ?>
        </div>
    </div>

    <div class="page-content">
        <!-- Panel Basic -->
        <div class="panel">
            <header class="panel-heading">
                <div class="panel-actions"></div>
                <h3 class="panel-title">Browse</h3>
            </header>
            <div class="panel-body">
                <div class="alert alert-info">
                    <h4 class='alert-heading'><?php e(lang('database_sql_query')); ?>:</h4>
                    <p><?php e($query); ?></p>
                </div>
                <?php if (empty($num_rows) || empty($rows) || !is_array($rows)) : ?>
                    <div class="alert alert-warning">
                        <?php e(lang('database_no_rows')); ?>
                    </div>
                <?php else : ?>
                    <p><?php echo e(sprintf(lang('database_total_results'), $num_rows)); ?></p>
                    <div class="admin-box">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <?php foreach ($rows[0] as $field => $value) : ?>
                                    <th><?php e($field); ?></th>
                                <?php endforeach; ?>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($rows as $row) : ?>
                                <tr>
                                    <?php foreach ($row as $key => $value) : ?>
                                        <td><?php e($value); ?></td>
                                    <?php endforeach; ?>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                <?php
                endif;
                ?>
            </div>
        </div>
    </div>
</div>
