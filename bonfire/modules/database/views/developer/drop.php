<div class="page">
    <div class="page-header">
        <h1 class="page-title"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= site_url('/') ?>">Home</a></li>
            <li class="breadcrumb-item active"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></li>
        </ol>
        <div class="page-header-actions">
            <?php Template::block('sub_nav') ?>
        </div>
    </div>

    <div class="page-content">
        <!-- Panel Basic -->
        <div class="panel">
            <header class="panel-heading">
                <div class="panel-actions"></div>
                <h3 class="panel-title">Drop Tables</h3>
            </header>
            <div class="panel-body">
                <div class='admin-box drop-table'>
                    <h3><?php echo lang('database_drop_title'); ?></h3>
                    <?php if (empty($tables) || !is_array($tables)) : ?>
                        <div class="alert alert-error">
                            <?php echo lang('database_drop_none'); ?>
                        </div>
                    <?php
                    else :
                        echo form_open(SITE_AREA . '/developer/database/drop');
                        ?>
                        <h4><?php echo lang('database_drop_confirm'); ?></h4>
                        <ul>
                            <?php foreach ($tables as $table) : ?>
                                <li><?php e($table); ?>
                                    <input type="hidden" name="tables[]" value="<?php e($table); ?>"/>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                        <div class="alert alert-warning">
                            <?php echo lang('database_drop_attention'); ?>
                        </div>
                        <fieldset class="form-actions">
                            <button type="submit" name="drop"
                                    class="btn btn-danger"><?php e(lang('database_drop_button')); ?></button>
                            <?php echo ' ' . lang('bf_or') . ' ' . anchor(SITE_AREA . '/developer/database', lang('bf_action_cancel')); ?>
                        </fieldset>
                        <?php
                        echo form_close();
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>