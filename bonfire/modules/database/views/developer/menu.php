<ul class="site-menu-sub">
	<li class="site-menu-item">
        <a class="animsition-link" href="<?php echo site_url(SITE_AREA . '/developer/database'); ?>">
            <span class="site-menu-title"><?php echo lang('bf_menu_db_maintenance'); ?></span>
        </a>
    </li>
    <li class="site-menu-item">
        <a class="animsition-link" href="<?php echo site_url(SITE_AREA . '/developer/database/backups'); ?>">
            <span class="site-menu-title"><?php echo lang('bf_menu_db_backup'); ?></span>
        </a>
    </li>
</ul>