<?php

$migrationsSegment = $this->uri->segment(3);
$checkSegment = $this->uri->segment(4);
$developerUrl = site_url(SITE_AREA . '/developer');

?>

<a class="btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon" data-toggle="tooltip"
   data-original-title="<?php echo lang('database_maintenance'); ?>"
   href='<?php echo "{$developerUrl}/database"; ?>'>
    <i class="icon fa fa-wrench" aria-hidden="true"></i>
</a>
<a class="btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon" data-toggle="tooltip"
   data-original-title="<?php echo lang('database_backups'); ?>"
   href='<?php echo "{$developerUrl}/database/backups"; ?>'>
    <i class="icon fa fa-archive" aria-hidden="true"></i>
</a>
<a class="btn btn-sm btn-sm btn-icon btn-primary btn-round btn-icon" data-toggle="tooltip"
   data-original-title="<?php echo lang('database_migrations'); ?>"
   href='<?php echo "{$developerUrl}/migrations"; ?>'>
    <i class="icon fa fa-cogs" aria-hidden="true"></i>
</a>