<div class="page">
    <div class="page-header">
        <h1 class="page-title"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= site_url('/') ?>">Home</a></li>
            <li class="breadcrumb-item active"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></li>
        </ol>
        <div class="page-header-actions">
            <?php Template::block('sub_nav') ?>
        </div>
    </div>

    <div class="page-content">
        <!-- Panel Basic -->
        <div class="panel">
            <header class="panel-heading">
                <div class="panel-actions"></div>
                <h3 class="panel-title">Maintenance</h3>
            </header>
            <div class="panel-body">
                <div class="admin-box database">
                    <?php if (empty($tables) || !is_array($tables)) : ?>
                        <div class="notification info">
                            <p><?php echo lang('database_no_tables'); ?></p>
                        </div>
                    <?php
                    else :
                        echo form_open(SITE_AREA . '/developer/database/');
                        ?>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th class="column-check">
                                    <div class="checkbox-custom checkbox-primary"><input class="check-all"
                                                                                         type="checkbox"/><label> </label>
                                    </div>
                                </th>
                                <th><?php echo lang('database_table_name'); ?></th>
                                <th class='records'><?php echo lang('database_num_records'); ?></th>
                                <th><?php echo lang('database_data_size'); ?></th>
                                <th><?php echo lang('database_index_size'); ?></th>
                                <th><?php echo lang('database_data_free'); ?></th>
                                <th><?php echo lang('database_engine'); ?></th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <td colspan="7">
                                    <div class="form-group form-material form-inline">
                                        <label for='database-action'><?php echo lang('bf_with_selected'); ?>:</label>
                                        <select name="action" id='database-action' class="col-md-3 form-control">
                                            <option value="backup"><?php echo lang('database_backup'); ?></option>
                                            <option value="repair"><?php echo lang('database_repair'); ?></option>
                                            <option value="optimize"><?php echo lang('database_optimize'); ?></option>
                                            <option>------</option>
                                            <option value="drop"><?php echo lang('database_drop'); ?></option>
                                        </select>
                                        <button type="submit" value="<?php echo lang('database_apply') ?>"
                                                class="btn btn-primary"><?php echo lang('database_apply') ?></button>
                                    </div>
                                </td>
                            </tr>
                            </tfoot>
                            <tbody>
                            <?php foreach ($tables as $table) : ?>
                                <tr>
                                    <td class="column-check">
                                        <div class="checkbox-custom checkbox-primary"><input type="checkbox"
                                                                                             value="<?php e($table->Name); ?>"
                                                                                             name="checked[]"/><label> </label>
                                        </div>
                                    </td>
                                    <td>
                                        <a href="<?php e(site_url(SITE_AREA . "/developer/database/browse/{$table->Name}")); ?>"><?php e($table->Name); ?></a>
                                    </td>
                                    <td class='records'><?php echo $table->Rows; ?></td>
                                    <td><?php e(is_numeric($table->Data_length) ? byte_format($table->Data_length) : $table->Data_length); ?></td>
                                    <td><?php e(is_numeric($table->Index_length) ? byte_format($table->Index_length) : $table->Index_length); ?></td>
                                    <td><?php e(is_numeric($table->Data_free) ? byte_format($table->Data_free) : $table->Data_free); ?></td>
                                    <td><?php e($table->Engine); ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php
                        echo form_close();
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>