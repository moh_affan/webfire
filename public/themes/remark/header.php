<?php

Assets::add_css([
    /*Core*/
    'bootstrap4.min.css',
    'bootstrap-extend.min.css',
    'site.min.css',
    'vendor/animsition/animsition.css',
    'vendor/asscrollable/asScrollable.css',
    'vendor/switchery/switchery.css',
    'vendor/intro-js/introjs.css',
    'vendor/slidepanel/slidePanel.css',
    'vendor/flag-icon-css/flag-icon.css',
    'vendor/waves/waves.css',
    /*datatable*/
    'vendor/datatables.net-bs4/dataTables.bootstrap4.css',
    'vendor/datatables.net-fixedheader-bs4/dataTables.fixedheader.bootstrap4.css',
    'vendor/datatables.net-fixedcolumns-bs4/dataTables.fixedcolumns.bootstrap4.css',
    'vendor/datatables.net-rowgroup-bs4/dataTables.rowgroup.bootstrap4.css',
    'vendor/datatables.net-scroller-bs4/dataTables.scroller.bootstrap4.css',
    'vendor/datatables.net-select-bs4/dataTables.select.bootstrap4.css',
    'vendor/datatables.net-responsive-bs4/dataTables.responsive.bootstrap4.css',
    'vendor/datatables.net-buttons-bs4/dataTables.buttons.bootstrap4.css',
    'tables/datatable.css',
    /*end datatable*/
    /*fonts*/
    'fonts/font-awesome/font-awesome.css',
    'fonts/material-design/material-design.min.css',
    'fonts/brand-icons/brand-icons.min.css',
    'fonts/Roboto/Roboto.min.css',
]);
if (isset($shortcut_data) && is_array($shortcut_data['shortcut_keys'])) {
    Assets::add_js($this->load->view('ui/shortcut_keys', $shortcut_data, true), 'inline');
}
?>
<!doctype html>
<html class="no-js css-menubar" lang="en">
<head>
    <meta charset="utf-8">
    <title><?php
        echo isset($toolbar_title) ? "{$toolbar_title} : " : '';
        e($this->settings_lib->item('site.title'));
        ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">
    <meta name="robots" content="noindex"/>
    <?php
    /* Modernizr is loaded before CSS so CSS can utilize its features */
    ?>
    <?php echo Assets::css(null, true); ?>
    <!--[if lt IE 9]>
    <script src="<?php echo Template::theme_url('vendor/html5shiv/html5shiv.min.js');?>"></script>
    <![endif]-->

    <!--[if lt IE 10]>
    <script src="<?php echo Template::theme_url('vendor/media-match/media.match.min.js'); ?>"></script>
    <script src="<?php echo Template::theme_url('vendor/respond/respond.min.js'); ?>"></script>
    <![endif]-->
    <script src="<?php echo Template::theme_url('vendor/breakpoints/breakpoints.js'); ?>"></script>
    <script>
        Breakpoints();
    </script>
</head>
<body class="animsition">
<!--[if lt IE 7]>
<p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different
    browser</a> or
    <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.
</p>
<![endif]-->
<noscript>
    <p>Javascript is required to use this page</p>
</noscript>
<!--    dari sini-->

<nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega" role="navigation">

    <div class="navbar-header">
        <button type="button" class="navbar-toggler hamburger hamburger-close navbar-toggler-left hided"
                data-toggle="menubar">
            <span class="sr-only">Toggle navigation</span>
            <span class="hamburger-bar"></span>
        </button>
        <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-collapse"
                data-toggle="collapse">
            <i class="icon md-more" aria-hidden="true"></i>
        </button>
        <div class="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu">
            <img class="navbar-brand-logo" src="<?= Template::theme_url('images/logo.png') ?>" title="<?=e($this->settings_lib->item('site.title'))?>">
            <span class="navbar-brand-text hidden-xs-down"> <?=e($this->settings_lib->item('site.title'))?></span>
        </div>
        <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-search" data-toggle="collapse">
            <span class="sr-only">Toggle Search</span>
            <i class="icon md-search" aria-hidden="true"></i>
        </button>
    </div>

    <div class="navbar-container container-fluid">
        <!-- Navbar Collapse -->
        <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
            <!-- Navbar Toolbar -->
            <ul class="nav navbar-toolbar">
                <li class="nav-item hidden-float" id="toggleMenubar">
                    <a class="nav-link" data-toggle="menubar" href="#" role="button">
                        <i class="icon hamburger hamburger-arrow-left">
                            <span class="sr-only">Toggle menubar</span>
                            <span class="hamburger-bar"></span>
                        </i>
                    </a>
                </li>
                <li class="nav-item hidden-sm-down" id="toggleFullscreen">
                    <a class="nav-link icon icon-fullscreen" data-toggle="fullscreen" href="#" role="button">
                        <span class="sr-only">Toggle fullscreen</span>
                    </a>
                </li>
                <li class="nav-item hidden-float">
                    <a class="nav-link icon md-search" data-toggle="collapse" href="#" data-target="#site-navbar-search"
                       role="button">
                        <span class="sr-only">Toggle Search</span>
                    </a>
                </li>
            </ul>
            <!-- End Navbar Toolbar -->

            <!-- Navbar Toolbar Right -->
            <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="javascript:void(0)" data-animation="scale-up"
                       aria-expanded="false" role="button">
                        <span class="flag-icon flag-icon-gb"></span>
                    </a>
                    <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                            <span class="flag-icon flag-icon-gb"></span> English</a>
                        <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                            <span class="flag-icon flag-icon-id"></span> Indonesia</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link navbar-avatar" data-toggle="dropdown" href="#" aria-expanded="false"
                       data-animation="scale-up"
                       role="button">
              <span class="avatar avatar-online">
                <img src="<?= Template::theme_url('portraits/5.jpg') ?>" alt="...">
                <i></i>
              </span>
                    </a>
                    <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="<?php echo site_url(SITE_AREA . '/settings/users/edit'); ?>"
                           role="menuitem"><i class="icon md-account"
                                              aria-hidden="true"></i>
                            Profile</a>
                        <a class="dropdown-item" href="<?php echo site_url('logout'); ?>" role="menuitem"><i
                                    class="icon md-power"
                                    aria-hidden="true"></i>
                            Logout</a>
                    </div>
                </li>
            </ul>
            <!-- End Navbar Toolbar Right -->
        </div>
        <!-- End Navbar Collapse -->

        <!-- Site Navbar Seach -->
        <div class="collapse navbar-search-overlap" id="site-navbar-search">
            <form role="search">
                <div class="form-group">
                    <div class="input-search">
                        <i class="input-search-icon md-search" aria-hidden="true"></i>
                        <input type="text" class="form-control" name="site-search" placeholder="Search...">
                        <button type="button" class="input-search-close icon md-close" data-target="#site-navbar-search"
                                data-toggle="collapse" aria-label="Close"></button>
                    </div>
                </div>
            </form>
        </div>
        <!-- End Site Navbar Seach -->
    </div>
</nav>

<div class="site-menubar">
    <div class="site-menubar-body">
        <div>
            <div>

                <?php echo Contexts::render_menu('text', 'normal'); ?>
                <!--
                <div class="site-menubar-section">
                    <h5>
                        Milestone
                        <span class="float-right">30%</span>
                    </h5>
                    <div class="progress progress-xs">
                        <div class="progress-bar active" style="width: 30%;" role="progressbar"></div>
                    </div>
                    <h5>
                        Release
                        <span class="float-right">60%</span>
                    </h5>
                    <div class="progress progress-xs">
                        <div class="progress-bar progress-bar-warning" style="width: 60%;" role="progressbar"></div>
                    </div>
                </div>
                -->
            </div>
        </div>
    </div>

    <!--<div class="site-menubar-footer">
        <a href="javascript: void(0);" class="fold-show" data-placement="top" data-toggle="tooltip"
           data-original-title="Profile">
            <span class="icon md-settings" aria-hidden="true"></span>
        </a>
        <a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Logout">
            <span class="icon md-power" aria-hidden="true"></span>
        </a>
    </div>-->
</div>

