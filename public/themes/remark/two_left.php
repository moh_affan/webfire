<?php

Assets::add_js(array(
    Template::theme_url('js/jwerty.js'),
),
    'external',
    true
);

echo theme_view('header');

?>
    <div class="page">
        <div class="page-aside">
            <div class="page-aside-switch">
                <i class="icon md-chevron-left" aria-hidden="true"></i>
                <i class="icon md-chevron-right" aria-hidden="true"></i>
            </div>
            <div class="page-aside-inner page-aside-scroll">
                <div data-role="container">
                    <div data-role="content">
                        <?php Template::block('sidebar'); ?>
                    </div>
                </div>
            </div>
            <!---page-aside-inner-->
        </div>
        <div class="page-main" style="margin-left: 260px">
            <div class="page-header">
                <h1 class="page-title"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= site_url(SITE_AREA) ?>">Home</a></li>
                    <li class="breadcrumb-item active"><?php echo isset($toolbar_title) ? "{$toolbar_title}" : '' ?></li>
                </ol>
                <div class="page-header-actions">
                    <?php Template::block('sub_nav') ?>
                </div>
            </div>
            <div class="page-content">
                <div class="panel">
                    <header class="panel-heading">
                        <div class="panel-actions"></div>
                        <h3 class="panel-title"></h3>
                    </header>
                    <div class="panel-body">
                        <?php
                        echo isset($content) ? $content : Template::content();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php echo theme_view('footer'); ?>