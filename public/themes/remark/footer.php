<?php
Assets::add_js([
    /*Core*/
    'vendor/babel-external-helpers/babel-external-helpers.js',
    'vendor/jquery/jquery.js',
    'vendor/popper-js/umd/popper.min.js',
    'vendor/bootstrap/bootstrap.js',
    'vendor/animsition/animsition.js',
    'vendor/mousewheel/jquery.mousewheel.js',
    'vendor/asscrollbar/jquery-asScrollbar.js',
    'vendor/asscrollable/jquery-asScrollable.js',
    'vendor/ashoverscroll/jquery-asHoverScroll.js',
    'vendor/waves/waves.js',
    /*Plugin*/
    'vendor/switchery/switchery.js',
    'vendor/intro-js/intro.js',
    'vendor/screenfull/screenfull.js',
    'vendor/slidepanel/jquery-slidePanel.js',
    'jwerty.js',
    /*datatables*/
    'vendor/datatables.net/jquery.dataTables.js',
    'vendor/datatables.net-bs4/dataTables.bootstrap4.js',
    'vendor/datatables.net-fixedheader/dataTables.fixedHeader.js',
    'vendor/datatables.net-fixedcolumns/dataTables.fixedColumns.js',
    'vendor/datatables.net-rowgroup/dataTables.rowGroup.js',
    'vendor/datatables.net-scroller/dataTables.scroller.js',
    'vendor/datatables.net-responsive/dataTables.responsive.js',
    'vendor/datatables.net-responsive-bs4/responsive.bootstrap4.js',
    'vendor/datatables.net-buttons/dataTables.buttons.js',
    'vendor/datatables.net-buttons/buttons.html5.js',
    'vendor/datatables.net-buttons/buttons.flash.js',
    'vendor/datatables.net-buttons/buttons.print.js',
    'vendor/datatables.net-buttons/buttons.colVis.js',
    'vendor/datatables.net-buttons-bs4/buttons.bootstrap4.js',
    'vendor/asrange/jquery-asRange.min.js',
    'vendor/bootbox/bootbox.js',
    /*Scripts*/
    'Component.js',
    'Plugin.js',
    'Base.js',
    'Config.js',
    'Section/Menubar.js',
    'Section/GridMenu.js',
    'Section/Sidebar.js',
    'Section/PageAside.js',
    'Plugin/menu.js',
    'config/colors.js',
    'config/tour.js',
    /*Page*/
    'Site.js',
    'Plugin/asscrollable.js',
    'Plugin/slidepanel.js',
    'Plugin/switchery.js',
    'Plugin/datatables.js',
    /*example*/
    'tables/datatable.js',
    'uikit/icon.js',
]);
?>
<!-- Footer -->
<footer class="site-footer">
    <div class="site-footer-legal">© 2018 <a
                href="http://themeforest.net/item/remark-responsive-bootstrap-admin-template/11989202">Remark</a></div>
    <div class="site-footer-right">
        Crafted with <i class="red-600 icon md-favorite"></i> by <a href="https://themeforest.net/user/creation-studio">Creation
            Studio</a>
    </div>
</footer>

<?php echo Assets::js(); ?>
<script>
    Config.set('assets','../../assets');
</script>
</body>
</html>
